<?php
include "./includes/header.php";
include "./includes/Cart.php";
include "./Paypal.php";

$cart = new Cart($db);
if(isset($_SESSION['login_user']))
{
$user = $db->prepare('SELECT * FROM utilisateur WHERE mail=?');
		$user->execute(array($_SESSION['login_user']));
		$userdata = $user->fetch(PDO::FETCH_OBJ);
}
if(!empty($_SESSION['cart']) && isset($_SESSION["login_user"])&& $userdata->nom!=""&& $userdata->prenom!=""&& $userdata->adresse!=""&& $userdata->zip!=""&& $userdata->pays!=""&& $userdata->ville!="")
{    $url = false;
    $paypal = new Paypal();
    $site =  'http://' . $_SERVER['SERVER_NAME'] . str_replace('index.php', '', "/techcity/");
    $params = [
        'RETURNURL' => $site . 'process.php',
        'CANCELURL' => $site . 'cancel.php',
        'PAYMENTREQUEST_0_AMT' => $_SESSION['total'], // 18 est la valeur total a payer
        'PAYMENTREQUEST_0_CURRENCYCODE' => 'EUR',
        'PAYMENTREQUEST_0_ITEMAMT' =>$_SESSION['total'], // pareil valeur total gros 
    ];
    $params = array_merge($params, $cart->getNames());
    $response = $paypal->request('SetExpressCheckout', $params);
    if($response){
        $url = 'https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&useraction=commit&token=' . $response['TOKEN'];
        $user = $_SESSION["login_user"];

?>

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Paiement</li>
				</ol>
			</div><!--/breadcrums-->

<!-- code a modid -->

<div class="table-responsive cart_info">      
        <div class="container">
        <h2><i class="fa fa-desktop color"></i> Information sur la commande</h2>
             <div class="row">
                <br />
                <form role="form" method="post" action="./checkout.php" >
                    <h4>Récapitulatif</h4>
                    <table class="table table-condensed">
                                <thead>
                                    <tr class="cart_menu">
                                        <td class="image">Objet</td>
                                        <td class="description"></td>
                                        <td class="price">Prix</td>
                                        <td class="quantity">Quantité</td>
                                        <td class="total">Total</td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                $keys = array_keys($_SESSION['cart']);
                                $reponse = $db->prepare('SELECT * FROM product WHERE id in ('.implode(", ",$keys).')');
                                $reponse->execute();
                                $data = $reponse->fetchAll(PDO::FETCH_OBJ);
                                $totaltva = 0;
                                foreach ($data as $item)
                                {
                                    
                                ?>
                                    <tr>
                                        <td class="cart_product">
                                            <a href=""><img src="<?php echo $item->url?>" alt="" width="40%"></a>
                                        </td>
                                        <td class="cart_description">
                                            <h4><a href=""><?php echo $item->name?></a></h4>
                                            <p id="webid" title="<?php echo $item->id ?>">Web ID: <?php echo $item->id?></p>
                                        </td>
                                        <td class="cart_price">
                                            <p><?php echo $item->price?>€</p>
                                        </td>
                                        <td class="cart_quantity">
                                            <div class="cart_quantity_button">
                                                <p name="quantity" id="<?php echo $item->id ?>"><?php echo $_SESSION['cart'][$item->id]?></p>
                                            </div>
                                        </td>
                                        <td class="cart_total">
                                        <?php $total = ($item->price)*($_SESSION['cart'][$item->id]); ?>
                                            <p class="cart_total_price"><?php echo $total?>€</p>
                                        </td>
                                    </tr>
                                <?php
                                $totaltva+=$total;
                                }
                                ?>
                                    
                                </tbody>
                            </table>
                      <div class="col-sm-4">
                        <address>
                            <strong><?= $userdata->nom . ' ' . $userdata->prenom; ?></strong><br>
                            <?= $userdata->adresse ; ?><br>
                            <?= $userdata->zip . ' ' . $userdata->ville; ?><br>
                            <?= $userdata->pays; ?><br>
                            <abbr title="Téléphone"><span class="fa fa-phone"></span></span> :</abbr> <?= $userdata->gsm; ?>.<br />
                        </address>
                    </div>
                    <div class="col-sm-4">
                        <div>
                            <p>Le total est de : <?= $totaltva ; ?>  €</p>
                            <a href="<?= $url; ?>" type="submit" class="btn btn-danger">Confirmer la commande</a>
                        </div>
                    </div><br>
                </form>

             </div>
        </div>
</div>
<!-- fin du code a modif -->

			
		</div>
	</section> <!--/#cart_items-->

	<?php } 
    }
	else{
        ?>
	<div class="container">
		<div class="table-responsive cart_info">
				<table class="table table-condensed" border="0" cellspacing="0" cellpadding="0">
				<tbody><tr>
					<td><h2>Completez vos information pour finalisez la commande !</h2></td>

			</tbody></table>   
		</div>
	</div>
<meta http-equiv="refresh" content="5; URL=./compte.php"><?php
	}
if(!isset($_SESSION['login_user']))
{
?>
	<div class="container">
		<div class="table-responsive cart_info">
				<table class="table table-condensed" border="0" cellspacing="0" cellpadding="0">
				<tbody><tr>
					<td><h2>Connectez vous pour commander !</h2></td>
					<td></td>
					<td rowspan="2">
                    <a href="./index.php">
						<img src="./images/cart/bon_plan_panier_vide.jpg" alt="Tous les bons plans du moment" border="0">
					</a>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit ex sed magna hendrerit imperdiet. Integer dignissim sem at ornare fermentum. Sed vestibulum malesuada enim, sed fringilla ligula lacinia vel. Aenean sagittis, odio sed porta consectetur, erat erat accumsan mauris, id tincidunt ex neque sed felis. Nam vel nisl vel orci gravida tempor. Nunc ac diam a dolor suscipit egestas non luctus velit. Duis consequat sem nec purus pellentesque, in molestie elit convallis. In vitae elementum nibh. Pellentesque tristique libero non velit pellentesque, eget molestie tortor sodales.
						</p>
						<p>Alors n'hésitez plus et <strong>découvrez vite ce nouvel univers !</strong></p>
					</td>
				</tr>
			</tbody></table>   
		</div>
	</div>
<meta http-equiv="refresh" content="5; URL=./login.php"><?php
}
include "./includes/footer.php";
?>