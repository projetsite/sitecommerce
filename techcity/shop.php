<?php
include "./includes/header.php";
include "./includes/filter.php";
if(empty($_GET['p']))$_GET['p']=1;
$filter = new filter($db,$_GET['p']);
?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						 <h2>Filtres</h2>
						<!--category-productsr-->
						<?php 
							include"./includes/categories.php";
						?>
							<!--category-productsr-->
					</div>
				</div>
                <?php
				//Affichage initial + affichage lorsque une option a été coché ou qu'un parametre a été mis dans l url
				if(!isset($_GET['marque'])){ $_GET['marque']="";}
				if(!isset($_GET['prix'])){ $_GET['prix']="";}
                $perPage = 12;
				if(isset($_GET['categorie']))
				{
					$arguments = array($_GET['marque'],$_GET['prix']);
					$reponse = $filter->filterALL($arguments);
				}
				if(!isset($_GET['cat']) && $_GET['categorie']!="all")
				{
					$arguments = array($_GET['categorie'],$_GET['marque'],$_GET['prix']);
					$reponse = $filter->filterSousCat($arguments);
				}
				
				if(isset($_GET['cat'])=="all" && $_GET['cat']!="")
				{
					$arguments = array($_GET['cat'],$_GET['marque'],$_GET['prix']);
					$reponse = $filter->filterCat($arguments);
				}
                ?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Catalogue</h2>
                        <?php
						// recupere et affiche les données
                        while($donnees = $reponse[0]->fetch()) {
                            ?>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <a href="product-details.php?id=<?php echo $donnees['id']?>"><img src="<?php echo $donnees['url']; ?>" alt="" /></a>
                                            <h2><?php echo $donnees['price']; ?>€</h2>
                                            <p><a href="product-details.php?id=<?php echo $donnees['id']?>"><?php echo $donnees['name']; ?></a></p>
                                            <a href="addtocart.php?id=<?php echo $donnees['id']?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au Panier</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                            
					</div><!--features_items-->
                    <ul class="pagination">
                                <?php // effectue la pagination
								if(isset($_GET['categorie']))
								{
                                $filter->pagination($reponse[1],$reponse[2],"categorie",$_GET['categorie'],$_GET['marque'],$_GET['prix']);
								}
								if(isset($_GET['cat']))
								{
                                $filter->pagination($reponse[1],$reponse[2],"cat",$_GET['cat'],$_GET['marque'],$_GET['prix']);
								}
                                ?>
                            </ul>
				</div>
			</div>
		</div>
	</section>
<?php
include "./includes/footer.php";
?>