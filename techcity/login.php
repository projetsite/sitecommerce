<?php
include "./includes/header.php";
$message="";
$message2="";
if(isset($_POST['Cmail'], $_POST['Cpassword'])!='')
{
	$userlocal = stripslashes($_POST['Cmail']);
	$passlocal = stripslashes($_POST['Cpassword']);
	//var_dump($_POST);
	if(isset($_GET["type"])&& $_GET["type"]=="login")
	{
		$reponse = $db->prepare('SELECT * FROM utilisateur WHERE mail=?');
		$reponse->execute(array($userlocal));
		$dn = $reponse->fetch(PDO::FETCH_ASSOC);
		if(!empty($dn)&&$dn['active'])
		{
			if(password_verify($passlocal, $dn['password'])) 
			{
						$_SESSION['login_user']=$userlocal; // Initialise Session
						if(isset($_SESSION["login_user"]))
						{
							?>
							<meta http-equiv="refresh" content="1; URL=./compte.php">
							<?php
						}
			} else {
				
						$message2 = " Erreur dans le mail ou le mdp ";
			}
		}
	}
}



//On verifie que le formulaire a ete envoye
if(isset($_POST['Email'], $_POST['Epassword'], $_POST['passwordverif'])!='')
{
    //On enleve lechappement si get_magic_quotes_gpc est active
    $_POST['Epassword'] = stripslashes($_POST['Epassword']);
    $_POST['Email'] = stripslashes($_POST['Email']);
	$pass = $_POST['Epassword'] ;
	$hash = password_hash($pass,PASSWORD_BCRYPT,['cost' => 13]) ;

    //On verifie si le mot de passe et celui de la verification sont identiques
    if($_POST['Epassword']==$_POST['passwordverif'])
    {

        //On verifie si le mot de passe a 6 caracteres ou plus
        if(strlen($_POST['Epassword'])>=6)
        {

            //On verifie si lemail est valide
            if(preg_match('#^(([a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+\.?)*[a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+)@(([a-z0-9-_]+\.?)*[a-z0-9-_]+)\.[a-z]{2,}$#i',$_POST['Email'])) {


                //On verifie s'il ny a pas deja un utilisateur inscrit avec le pseudo choisis
                $reponse = $db->prepare('SELECT id FROM utilisateur WHERE mail = ?');
                $reponse->execute(array($_POST['Email']));
                $dn = $reponse->fetch(PDO::FETCH_ASSOC);
				$token = md5(time());
                $result = $db->prepare("INSERT INTO utilisateur (pseudo,mail, password, date, token) VALUES (:name,:email, :password, NOW(), :token)");

                if(empty($dn))
                {
                    //On enregistre les informations dans la base de donnee
                    try
                    {
                        $result->execute(array('name'=>$_POST['Epseudo'],'email' => $_POST['Email'],'password' => $hash, 'token'=> $token));
						$reponse = $db->prepare('SELECT id FROM utilisateur WHERE mail = ?');
                		$reponse->execute(array($_POST['Email']));
                		$dn = $reponse->fetch(PDO::FETCH_OBJ);		
						// puis envoie du mail si try OK
				$to  = $_POST['Email'];
		
				// Sujet
				$subject = 'TechCity';
		
				// message
		
				$link =  'http://' . $_SERVER['SERVER_NAME'] . "activate.php?id=$dn->id&amp;token=$token";
				$message = "<html><head><title>TechCity</title></head><body><p>Merci pour votre inscription sur TechCity !</p><p>Pour activer votre compte, merci de cliquer sur le lien <a href=\"$link\">D'activation</a> ou collez ce lien dans votre barre d'adresse : $link</p></body></html>";
		
				// Pour envoyer un mail HTML, l'en-tête Content-type doit être défini
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
				// En-têtes additionnels
				$headers .= 'To: '. $_POST['Epseudo'] . ' '.  $_POST['Epseudo'] . '  <' . $_POST['Email'] . '>' . '\r\n';
				$headers .= 'From: TechCity <samy.delsarte@gmail.com>' . '\r\n';
		
				// Envoi
				mail($to, $subject, $message, $headers);

			 			$message= "Vous avez bien été inscrit. Vous pouvez dorénavant vous connecter.";

                    }
                    catch(MySQLException $e)
                    {
                        //Sinon on dit qu'il y a eu une erreur
						$message = 'Une erreur est survenue lors de l\'inscription.';
                    }
                }
                else
                {
                    //Sinon, on dit que le pseudo voulu est deja pris
                    $message= 'Un autre utilisateur utilise d&eacute;j&agrave; le nom d\'utilisateur que vous d&eacute;sirez utiliser.';
                }
            }
            else
            {
                //Sinon, on dit que lemail nest pas valide
				 $message = 'L\'email que vous avez entr&eacute; n\'est pas valide.';
            }
        }
        else
        {
            //Sinon, on dit que le mot de passe nest pas assez long
			 $message = 'Le mot de passe que vous avez entr&eacute; contien moins de 6 caract&egrave;res.';
        }
    }

    else
    {
        //Sinon, on dit que les mots de passes ne sont pas identiques
		$message = 'Les mots de passe que vous avez entr&eacute; ne sont pas identiques.';
    }
}
else
{

}


?>

	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Connexion à votre compte</h2>
						<form action="login.php?type=login" method="post">
							<input type="email" placeholder="Email" name="Cmail" required="required"/>
							<input type="password" placeholder="Mot de passe" name="Cpassword" required="required"/>
							<button type="submit" class="btn btn-default">Connexion</button>
						</form>
                        <?php if(!empty($message2)){ ?>
                        <br>
                            <div class="alert alert-warning">
                            <button class="close" data-dismiss="alert" type=
                            "button">×</button> <strong>Erreur d'acces!</strong>
                            <?php echo $message2 ?>
                        </div><?php } ?>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OU</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Enregistrez-vous!</h2>
						<form action="login.php?type=register" method="post">
							<input type="text" placeholder="Pseudo" name="Epseudo" required="required"/>
							<input type="email" placeholder="Email" name="Email" required="required"/>
							<input type="password" placeholder="Mot de passe : 6 carracteres" name="Epassword" required="required"/>
							<input type="password" placeholder="Verification du mot de passe" name="passwordverif" required="required"/>
                            <div>
                            En cliquant sur <strong>S'enregistrer</strong>, vous acceptez <a href="#" data-toggle="modal" data-target="#t_and_c_m">les conditions d'utilisations</a> de notre site.
                        	</div><br>
							<button type="submit" class="btn btn-default">S'enregistrer</button>
                            <br>
                            <div class="alert alert-info"><strong>Confirmation:</strong>
                            Un email de confirmation vous sera envoyé.<br>
                        	</div>
						</form>
                        <br>
                        <p><?php echo $message ?></p>
					</div><!--/sign up form-->
				</div>
			</div>
               <!-- Modalalite -->
                <div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">Conditions</h4>
                            </div>
                            <div class="modal-body">
                                <p>Pas de condition spécial, juste du made in china ma guelle !!</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Quitter</button>
                            </div>
                        </div>
                    </div>
                </div><!-- Fin de Modalalite -->
		</div>
	</section><!--/form-->
	<?php
include "./includes/footer.php";
?>