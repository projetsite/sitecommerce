<?php
include "./includes/header.php";
//var_dump($_SESSION);
		$message="";
		$reponse = $db->prepare('SELECT * FROM utilisateur WHERE mail=?');
		$reponse->execute(array($_SESSION['login_user']));
		$data = $reponse->fetch(PDO::FETCH_OBJ);
		
		//var_dump($data->pseudo);
   if(!empty($_POST))
		{
			$password = stripslashes($_POST['password']);
			$passwordverif = stripslashes($_POST['passwordverif']);
			//UPDATE `eshop`.`utilisateur` SET `prenom`='aze' WHERE  `mail`='lulu@huhi.com' LIMIT 1;
			if(empty($password) && empty($passwordverif))
			{
			$sql ="UPDATE utilisateur SET nom=? , prenom=?, adresse=?, zip=? , gsm=?, pays=?, ville=?, infos=? WHERE mail=?";
			$result = $db->prepare($sql);
			$result->execute(array($_POST['nom'],$_POST['prenom'],$_POST['adresse'],$_POST['zip'],$_POST['gsm'],$_POST['pays'],$_POST['ville'],$_POST['message'],$_SESSION['login_user']));
			?>
			<meta http-equiv="refresh" content="1; URL=./compte.php">
			<?php
			}else{
				if(strlen($_POST['password'])>=6)
				{
					if($password==$passwordverif)
					{
						$hash = password_hash($password,PASSWORD_BCRYPT,['cost' => 13]) ;
						$sql ="UPDATE utilisateur SET password=? , nom=? , prenom=?, adresse=?, zip=? , gsm=?, pays=?, ville=? WHERE mail=?";
						$result = $db->prepare($sql);
						$result->execute(array($hash,$_POST['nom'],$_POST['prenom'],$_POST['adresse'],$_POST['zip'],$_POST['gsm'],$_POST['pays'],$_POST['ville'],$_SESSION['login_user']));
						?>
						<meta http-equiv="refresh" content="1; URL=./compte.php">
						<?php
					}else{
						$message="Les mots de passe ne sont pas identiques";
					}
				}else{
					$message="Le mots de passe doit faire 6 caractere minimum";
				}
			}
		}
		
?>

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Information</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Données</h2>
			</div>


			<div class="register-req">
				<p>Vous pouvez modifier vos informations. L'email et le pseudo ne sont pas modifiable .</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
            <form action="compte.php" method="post" id="form1">
				<div class="row">
					<div class="col-sm-3">
						<div class="shopper-info">
							<p>Informations d'utilisateur</p>
								<p>Pseudo:</p>
								<input type="text" value="<?php echo $data->pseudo; ?>" readonly>
                                <p>Mail:</p>
                                <input type="text" value="<?php echo $data->mail; ?>" readonly>
                                <p>Nouveau mot de passe:</p>
								<input type="password" placeholder="Mot de passe" name="password">
                                <p>Confirmation:</p>
								<input type="password" placeholder="Confirmation du mot de passe" name="passwordverif">
                                <?php if(!empty($message)){ ?>
                                <br>
                            	<div class="alert alert-info">
                            	<?php echo $message ?><br>
                        		</div><?php } ?>
							
						</div>
					</div>
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<p>Adresse de livraison</p>
							<div class="form-one">
									<p>Nom:</p>
									<input type="text" placeholder="Nom "  value="<?php echo $data->nom; ?>"  name="nom" required="required">
                                    <p>Prenom:</p>
									<input type="text" placeholder="Prénom " value="<?php echo $data->prenom; ?>" name="prenom" required="required">
                                    <p>Adresse:</p>
									<input type="text" placeholder="Adresse " value="<?php echo $data->adresse; ?>" name="adresse" required="required">
                                    <p>Ville:</p>
                                    <input type="text" placeholder="Ville " value="<?php echo $data->ville; ?>" name="ville" required="required">
                                    <p>Zip:</p>
                                    <input type="text" placeholder="Code postal " value="<?php echo $data->zip; ?>" name="zip" required="required">
                                    <p>Gsm:</p>
                                    <input type="text" placeholder="Téléphone " value="<?php echo $data->gsm; ?>" name="gsm">
									<button type="submit" class="btn btn-primary">Sauvegarder</button>
								
							</div>
							<div class="form-two">
									<p>Pays:</p>
									<select name="pays">
										<option>-- Pays --</option>
										<option value="Belgique" <?= isset($data->pays) && $data->pays === "Belgique" ? "selected" : ''; ?> >Belgique</option>
										<option value="France" <?= isset($data->pays) && $data->pays === "France" ? "selected" : ''; ?> >France</option>
										<option value="Suisse" <?= isset($data->pays) && $data->pays === "Suisse" ? "selected" : ''; ?> >Suisse</option>
										<option value="Luxembourg" <?= isset($data->pays) && $data->pays === "Luxembourg" ? "selected" : ''; ?> >Luxembourg</option>
									</select>
								
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="order-message">
							<p>Info supplémentaires</p>
							<textarea name="message"  placeholder="Notes à propos de votre adresse, immeuble, etc." rows="16"><?php echo $data->infos;?></textarea>
						</div>
					</div>
				</div>
			</div>
            </form>
            <br>
		</div>
	</section> <!--/#cart_items-->

	<?php
include "./includes/footer.php";
?>