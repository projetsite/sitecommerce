<?php
include "./includes/header.php";
?>
	
	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-15">
                                    <a href="./shop.php?categorie=all&prix=100"><img src="images/home/encar1.jpg" class="girl img-responsive" alt="" /></a>
								</div>
							</div>
							<div class="item">
								<div class="col-sm-15">
                                    <a href="./product-details.php?id=46"><img src="images/home/encar2.jpg" class="girl img-responsive" alt=""/></a>
								</div>
							</div>
							
							<div class="item">
								<div class="col-sm-6">
									<h1><span>T</span>echCity</h1>
									<h2>NZXT Phantom 820</h2>
									<p>Installez une configuration ultra-performante dans le boîtier NZXT Phantom 820 ! A la fois élégant, performant et pratique, il est conçu pour répondre aux besoins et aux envies des gamers. L'achat de ce boîtier NZXT haut de gamme est un pas de plus vers la victoire !</p>
									<a href="./product-details.php?id=30"><button type="button" class="btn btn-default get" >Acheter</button></a>
								</div>
								<div class="col-sm-6">
									<img src="images/home/encar3.jpg" class="girl img-responsive" alt="" />
									<img src="images/home/price.png" class="pricing" alt="" />
								</div>
							</div>
							
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-15 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Nouveaux Produits</h2>
                        
                         <?php

							$reponse = $db->query('SELECT * FROM product ORDER BY ID DESC LIMIT 0, 4;');
						//var_dump($_SESSION);
							while($donnees = $reponse->fetch())
							{
								?>
						<div class="col-sm-3">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<a href="product-details.php?id=<?php echo $donnees['id']?>"><img src="<?php echo $donnees['url']; ?>" alt="" /></a>
                                            <h2><?php echo $donnees['price']; ?>€</h2>
											<p><a href="product-details.php?id=<?php echo $donnees['id']?>"><?php echo $donnees['name']; ?></p>
											<a href="addtocart.php?id=<?php echo $donnees['id']?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
                                    <img src="images/home/new.png" class="new" alt="">
								</div>
							</div>
						</div>
                        <?php
						}
				?>
						
					</div><!--features_items-->

					
				</div>
			</div>
		</div>
	</section>
	
	<?php
include "./includes/footer.php";
?>