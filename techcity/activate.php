<?php
include "./includes/header.php";
if(isset($_GET['id'], $_GET['token'])){
    $reponse = $db->prepare('SELECT * FROM utilisateur WHERE id=? AND token=?');
	$reponse->execute(array($_GET['id'],$_GET['token']));
	$dn = $reponse->fetch(PDO::FETCH_OBJ);
    if($dn){
        	$sql ="UPDATE utilisateur SET active=? WHERE id=?";
			$result = $db->prepare($sql);
			$result->execute(array(1,$_GET['id']));
			?>
			<div class="page-title">
				<div class="container">
					<h2><i class="fa fa-shopping-cart color"></i> Vous venez d'activer votre compte !</h2>
					<hr />
					<h5><a class="btn btn-success" href="./login.php">Retourner a la page d'identification</a></h5>
				</div>
			</div>
			<?php
    }
}
?>