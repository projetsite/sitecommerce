<?php
include "./Paypal.php";
include "./includes/header.php";
include "./includes/Cart.php";
$cart = new Cart($db);
$paypal = new Paypal();
$response = $paypal->request('GetExpressCheckoutDetails', ['TOKEN' => $_GET['token']]);
$errors = array();
if($response){
    if($response['CHECKOUTSTATUS'] != 'PaymentActionCompleted'){
        //preparation de l'envoieé;
        $params = [
            'TOKEN' => $_GET['token'],
            'PAYERID' => $_GET['PayerID'],
            'PAYMENTREQUEST_0_AMT' => $response['AMT'],
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'EUR',
            'PAYEMENTACTION' => 'Sale'
        ];
        $params = array_merge($params, $cart->getNames());
        $response = $paypal->request('DoExpressCheckoutPayment', $params);
        if($response){
			
			$result = $db->prepare("INSERT INTO orders (user_id,amount) VALUES (:user_id,:amount)");
			$result->execute(array('user_id'=>$_SESSION['login_user'],'amount' => $response['PAYMENTINFO_0_AMT']));
        } else {
            $errors = $paypal->errors;
            if (!isset($errors['PAYMENTINFO_0_TRANSACTIONID'])) {
                var_dump($errors);
                $count = (count($errors) - 7) / 2;
                for ($i = 0; $i < $count; $i++) {
                    $error[] = $errors["L_LONGMESSAGE$i"];
                }
                $error = implode('& ', $error);
               //db Error 
            }
        }
    }
    unset($_SESSION['cart']);
	$_SESSION['total']=0;
} else {
    $errors = $paypal->errors;
}

if(!$errors): ?>

    <div class="page-title">
        <div class="container">
            <h2><i class="fa fa-shopping-cart color"></i> Nous avons bien reçu votre commande !</h2>
            <hr />
            <h5>Merci d'avoir acheté dans notre boutique !</h5>
            <h5>Votre numéro de commande est le  <span class="color"><?= $response['PAYMENTINFO_0_TRANSACTIONID']; ?></span>. Gardez-le précieusement pour toute autre aide nécessaire.</h5>
            <div class="sep-bor"></div>
        </div>
    </div>
<?php else: ?>
    <div class="container">
        <div class="alert alert-danger">Une erreur s'est produite. Votre compte Paypal n'a pas été débité.<br><a class="btn btn-primary" href="./">Retourner à l'accueil</a></div>
    </div>
<?php endif;