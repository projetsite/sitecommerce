<?php // connexion a la DB via le fichier sql.ini
		include "sql.php";
		$config = $array;
		$username = $config['username'];
		$password = $config['password'];
		$database = $config['dbname'];
		$host = $config['host'];
		$port = $config['port'];
		$driver = $config['driver'];

		$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");

		try {
			$db = new PDO($driver . ':host=' . $host . ';port=' . $port . ';dbname=' . $database, $username, $password, $options);
		} catch (Exception $e) {
			die('Impossible de se connecter &agrave; la base de donn&eacute;e !');
		}

        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>