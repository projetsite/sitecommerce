 <?php
 class Cart{
	 private $db;
 public function __construct($db){
        $this->db = $db;
    }
	
 public function getItems(){ // recupere les object contenu dans la sessions pour l'envoie vers paypal
        $keys = array_keys($_SESSION['cart']);
        $sql = $this->db->prepare('SELECT * FROM product WHERE id IN (' . implode(', ', $keys) . ')');
        $sql->execute();
        return $sql->fetchall(PDO::FETCH_OBJ);
    }
	
	public function getNames(){ // recupere les nom via getItems et les attibues dans un array pour l'envoie a paypal
        $items = $this->getItems();
        $params = array();
        foreach($items as $key => $item){
            $params["L_PAYMENTREQUEST_0_NAME$key"] = $item->name;
            $params["L_PAYMENTREQUEST_0_DESC$key"] = $item->marque;
            $params["L_PAYMENTREQUEST_0_AMT$key"] = $item->price;
            $params["L_PAYMENTREQUEST_0_QTY$key"] = $_SESSION['cart'][$item->id];
        }
        return $params;
    }
 }
?>