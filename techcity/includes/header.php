<?php
session_start();
require_once("Database.php"); // besoin de database.php
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | TechCity</title>
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/animate.css" rel="stylesheet">
	<link href="./css/main.css" rel="stylesheet">
	<link href="./css/responsive.css" rel="stylesheet">
    <link rel="icon" href="./images/favicon.ico" />
</head><!--/head-->

<body>
	<header id="header"><!--header-->

		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.php"><img src="./images/home/logo1.png" alt="" /></a>
						</div>
						<div class="btn-group pull-right"> <!--espace necessaire -->
							<div class="btn-group">
							</div>
							<div class="btn-group"><!--espace necessaire -->
							</div>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
                            	<?php
									if(isset($_SESSION["login_user"]))
									{
								?>
									<li><a href="compte.php"><i class="fa fa-user"></i> Compte</a></li>
                                <?php
                                	}
								?>
                                <?php
									if(!isset($_SESSION["login_user"]))
									{
								?>
									<li><a href="login.php"><i class="fa fa-crosshairs"></i> S'enregistrer</a></li>
                                <?php
                                	}
								?>
                                <?php
								if(!empty($_SESSION['cart']))
								{
								?>
								<li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Panier - <?php if(isset($_SESSION['cart'])){ echo count($_SESSION['cart']);}else{ echo 0;} ?> articles | <?php echo $_SESSION['total'] ?> € </a></li>
                                <?php
								}
                                if(isset($_SESSION["login_user"]))
                                {
									?>
									<li><a href="logout.php"><i class="fa fa-lock"></i> Déconnexion</a></li>
                                <?php
                                }else{
									?>
								<li><a href="login.php"><i class="fa fa-lock"></i> Connexion</a></li>
                                <?php
                                }
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php" class="active">Home</a></li>
								<li><a href="shop.php?categorie=all">Catalogue</a></li>
                                <li class="dropdown"><a href="shop.php?cat=Composants_PC">Composants PC<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.php?categorie=HDD">Disque durs</a></li>
                                        <li><a href="shop.php?categorie=SSD">SSD</a></li>
                                        <li><a href="shop.php?categorie=CG">Cartes graphiques</a></li>
                                        <li><a href="shop.php?categorie=Proc">Processeurs</a></li>
                                        <li><a href="shop.php?categorie=RAM">RAM</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="shop.php?cat=Boitier">Boitiers<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.php?categorie=BoitierPC">Boitiers PC</a></li>
                                        <li><a href="shop.php?categorie=BoitierHDD">Boitiers pour disque dur</a></li>
                                        <li><a href="shop.php?categorie=Routeur">Routeurs</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="shop.php?cat=Alimentation">Alimentations<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.php?categorie=Alimentation">Boitiers d'alimentation</a></li>
                                        <li><a href="shop.php?categorie=Cable">Câbles d'alimentation</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact-us.php">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->