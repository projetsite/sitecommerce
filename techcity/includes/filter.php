 <?php
 class filter{
	private $db;
 	public function __construct($db,$p){
    $this->db = $db;
	$this->p = $p;
    }
	public function filtreMarque($categorie){ // filtre option marque d'une catégorie
					$req = $this->db->query("SELECT * FROM product WHERE categorie='$categorie'");
                    $data = $req->fetchall(PDO::FETCH_ASSOC);
					return $data;
	}
	public function filtreMarqueAll(){ //filtre option toutes marques
					$req = $this->db->query("SELECT * FROM product");
                    $data = $req->fetchall(PDO::FETCH_ASSOC);
					return $data;
	}
	public function filtreMarqueCat($cat){ // filtre option d'une sous catégorie
					$req = $this->db->query("SELECT * FROM product WHERE catGeneral='$cat'");
                    $data = $req->fetchall(PDO::FETCH_ASSOC);
					return $data;
	}
 	public function filterALL($categorie){ // requete exécuter pour filtre ce qui est charger apres avoir cliqué sur une option dans Catalogue
					$sql="SELECT COUNT(id) FROM product";
					$sql2 = "SELECT * FROM product";
					if($categorie[0]!="")
					{
						 $sql=$sql." WHERE marque='$categorie[0]'";
						 $sql2=$sql2." WHERE marque='$categorie[0]'";
					}
					if($categorie[1]!="")
					{
						if($categorie[0]!=""){ $sql=$sql." AND "; $sql2=$sql2." AND ";}
						else{$sql=$sql." WHERE "; $sql2=$sql2." WHERE ";}
						if($categorie[1] <= 200 ){$minimum = $categorie[1]-50;}else{$minimum = $categorie[1]-50; $categorie[1]=50000;}
						 $sql=$sql."(price BETWEEN ".$minimum ." AND ".$categorie[1].")";
						 $sql2=$sql2."(price BETWEEN ".$minimum ." AND ".$categorie[1].")";
					}
	 				$perPage = 12;
        			$req = $this->db->query($sql);
                    $data = $req->fetch((PDO::FETCH_NUM));
                    $nbProd = $data[0];
                    $nbPage = ceil($nbProd/$perPage);
                    if(isset($this->p) && $this->p>0 && $this->p<=$nbPage){
                        $cPage = $this->p;
                    }
                    else{
                        $cPage =1;
                    }
                    $reponse = $this->db->query($sql2." ORDER BY id DESC LIMIT ".(($cPage-1)*$perPage).",$perPage");
					$array = array($reponse,$cPage,$nbPage);
					return $array;
    }
	
	public function filterSousCat($catselectionner){ // requete exécuter pour filtre ce qui est charger apres avoir cliqué sur une option dans une sous catégorie
					$sql="SELECT COUNT(id) FROM product WHERE categorie='$catselectionner[0]'";
					$sql2 = "SELECT * FROM product WHERE categorie='$catselectionner[0]'";
					if($catselectionner[2]!="")
					{
						if($catselectionner[2] <= 200 ){$minimum = $catselectionner[2]-50;}else{$minimum = $catselectionner[2]-50; $catselectionner[2]=50000;}
						 $sql=$sql." AND (price BETWEEN ".$minimum ." AND ".$catselectionner[2].")";
						 $sql2=$sql2." AND (price BETWEEN ".$minimum ." AND ".$catselectionner[2].")";
					}
					if($catselectionner[1]!="")
					{
						 $sql=$sql." AND marque='$catselectionner[1]'";
						 $sql2=$sql2." AND marque='$catselectionner[1]'";
					}
					else{
						
					}
					$perPage = 12;
					$req = $this->db->query($sql);
                    $data = $req->fetch((PDO::FETCH_NUM));
                    $nbProd = $data[0];
                    $nbPage = ceil($nbProd/$perPage);
                    if (isset($this->p) && $this->p > 0 && $this->p <= $nbPage) {
                            $cPage = $this->p;
					} else {
						$cPage = 1;
					}
                    $reponse = $this->db->query($sql2." ORDER BY id DESC LIMIT " . (($cPage - 1) * $perPage) . ",$perPage");
					$array = array($reponse,$cPage,$nbPage);
					return $array;
	}
	public function filterCat($categorie){ // requete exécuter pour filtre ce qui est charger apres avoir cliqué sur une option dans une catégorie
					$sql="SELECT COUNT(id) FROM product WHERE catGeneral='$categorie[0]'";
					$sql2 = "SELECT * FROM product WHERE catGeneral='$categorie[0]'";
					if($categorie[2]!="")
					{
						if($categorie[2] <= 200 ){$minimum = $categorie[2]-50;}else{$minimum = $categorie[2]-50; $categorie[2]=50000;}
						 $sql=$sql." AND (price BETWEEN ".$minimum ." AND ".$categorie[2].")";
						 $sql2=$sql2." AND (price BETWEEN ".$minimum ." AND ".$categorie[2].")";
					}
					if($categorie[1]!="")
					{
						 $sql=$sql." AND marque='$categorie[1]'";
						 $sql2=$sql2." AND marque='$categorie[1]'";
					}
					

					$perPage = 12;
					$req = $this->db->query($sql);
                    $data = $req->fetch((PDO::FETCH_NUM));
                    $nbProd = $data[0];
                    $nbPage = ceil($nbProd/$perPage);
                    if (isset($this->p) && $this->p > 0 && $this->p <= $nbPage) {
                            $cPage = $this->p;
					} else {
						$cPage = 1;
					}
                    $reponse = $this->db->query( $sql2."  ORDER BY id DESC LIMIT " . (($cPage - 1) * $perPage) . ",$perPage");
					$array = array($reponse,$cPage,$nbPage);
					return $array;
	}
	public function pagination($repUN,$repDeux,$key,$categorie,$marque,$prix){ // gestion de la pagination $repun c est page actuel, $repDeux c est le nombre de pages, $key c est la catégorie selectionner, $categorie c est la valeur de la catégorie 
					$emplacement = $_SERVER['PHP_SELF']."?".$key."=".$categorie."&marque=".$marque."&prix=".$prix;
					if ($repUN > 1 ){
						$page = $repUN - 1;
						$prev = " <li><a href=\"$emplacement&p=$page\">&laquo;</a></li> ";
					}
					else{
						$prev = "<li><a href=>&laquo;</a></li>";
	
					}
					echo $prev;
							for($i=1;$i<=$repDeux;$i++){
							  if($i==$repUN){
								  echo "<li class=\"active\"><a href=\"$emplacement&p=$i\">$i</a></li>";
							  }
								else{
									echo "<li><a href=\"$emplacement&p=$i\">$i</a></li>";
								}
							}
					if($repUN < $repDeux){
						$page = $repUN + 1;
						$next = "<li><a href=\"$emplacement&p=$page\">&raquo;</a></li>";
					}
					else{
						$next = "<li><a href=>&raquo;</a></li>";
					}
					echo $next;
	}
 }
?>