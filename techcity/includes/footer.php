<footer id="footer"><!--Footer-->
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2014 ISIMs. All rights reserved.</p>
					<p class="pull-right">Designed by <span>Delsarte Samy & Meyers Maxime</span></p>
				</div>
			</div>
		</div>
	</footer><!--/Footer-->
	

  
    <script src="./js/jquery.js"></script>
	<script src="./js/bootstrap.min.js"></script>
	<script src="./js/jquery.scrollUp.min.js"></script>
	<script src="./js/price-range.js"></script>
    <script src="./js/main.js"></script>
</body>
</html>