<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
										<ul>
                                            <span>Marque</span>
                                            <FORM>
                                                <SELECT name="marque" size="1" onChange="filtreMarque()" id="marque">
                                                <?php // code qui permet d'executer la bonne requete en fonction de la categorie ou l'on est
												if(isset($_GET['categorie'])&& $_GET['categorie']!="all")
												{	
													$data = $filter->filtreMarque($_GET['categorie']);
												}
												if(isset($_GET['categorie']) && $_GET['categorie']=="all")
												{
													$data = $filter->filtreMarqueALL();
												}
												if(isset($_GET['cat']))
												{
													$data = $filter->filtreMarqueCat($_GET['cat']);
												}
												?><OPTION value="all">Toutes marques</OPTION><?php
													foreach($data as $item) {
														$states[] = $item['marque'];
													}
													$states = array_unique($states);
													foreach($states as $items)
													{
                                                    		?><OPTION value="<?php echo $items ;?>" <?= isset($_GET['marque']) && $_GET['marque'] === $items ? "selected" : ''; ?>><?php echo $items ;?></OPTION><?php
													}
													?>
                                                </SELECT>
                                            </FORM>
                                            <span>Prix</span>
                                            <FORM>
                                                <SELECT name="mark" size="1" onChange="filtrePRIX()" id="prix">
                                                	<OPTION value="" <?= isset($_GET['prix']) && $_GET['prix'] === "" ? "selected" : ''; ?>>Tout
                                                    <OPTION value="50" <?= isset($_GET['prix']) && $_GET['prix'] === "50" ? "selected" : ''; ?>>0€ à 50€
                                                    <OPTION value="100" <?= isset($_GET['prix']) && $_GET['prix'] === "100" ? "selected" : ''; ?>>50€ à 100€
                                                    <OPTION value="150" <?= isset($_GET['prix']) && $_GET['prix'] === "150" ? "selected" : ''; ?>>100€ à 150€
                                                    <OPTION value="200" <?= isset($_GET['prix']) && $_GET['prix'] === "200" ? "selected" : ''; ?>>150€ à 200€
                                                    <OPTION value="250" <?= isset($_GET['prix']) && $_GET['prix'] === "250" ? "selected" : ''; ?>>200€ et +
                                                </SELECT>
                                            </FORM>
										</ul>
							</div>
</div><!--/category-products-->
<script>
	function filtrePRIX(){
					var url = document.URL;
					var prix = document.getElementById("prix").value;
					url = modURLParam(url, 'prix', prix);
					url = modURLParam(url, 'p', 1);
					window.location.href = url;
	}
	function filtreMarque() {
					// Initial URL
					var url = document.URL;
					var marque = document.getElementById("marque").value;
					url = modURLParam(url, 'p', 1);
					if( marque == "all")
					{
						url = modURLParam(url, 'marque', "");
						window.location.href = url;
					}else
					{
						url = modURLParam(url, 'marque', marque);
						window.location.href = url;
					}
	}
	
	
	<!--/Fonction de remplacement de parametre dans l url-->
	(function(expCharsToEscape, expEscapedSpace, expNoStart, undefined) {
  modURLParam = function(url, paramName, paramValue) {
    paramValue = paramValue != undefined
      ? encodeURIComponent(paramValue).replace(expEscapedSpace, '+')
      : paramValue;
    var pattern = new RegExp(
      '([?&]'
      + paramName.replace(expCharsToEscape, '\\$1')
      + '=)[^&]*'
    );
    if(pattern.test(url)) {
      return url.replace(
        pattern,
        function($0, $1) {
          return paramValue != undefined ? $1 + paramValue : ''; 
        }
      ).replace(expNoStart, '$1?');
    }
    else if (paramValue != undefined) {
      return url + (url.indexOf('?') + 1 ? '&' : '?')
        + paramName + '=' + paramValue;
    }
    else {
      return url;
    }
  };
})(/([\\\/\[\]{}().*+?|^$])/g, /%20/g, /^([^?]+)&/);
</script>