<?php
include "./includes/header.php";
?>
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Panier</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
            	<?php if(!empty($_SESSION['cart'])) { ?>
                            <table class="table table-condensed">
                                <thead>
                                    <tr class="cart_menu">
                                        <td class="image">Objet</td>
                                        <td class="description"></td>
                                        <td class="price">Prix</td>
                                        <td class="quantity">Quantité</td>
                                        <td class="total">Total</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                $keys = array_keys($_SESSION['cart']);
                                $reponse = $db->prepare('SELECT * FROM product WHERE id in ('.implode(", ",$keys).')');
                                $reponse->execute();
                                $data = $reponse->fetchAll(PDO::FETCH_OBJ);
                                $totaltva = 0;
                                foreach ($data as $item)
                                {
                                    
                                ?>
                                    <tr>
                                        <td class="cart_product">
                                            <a href="product-details.php?id=<?php echo $item->id?>"><img src="<?php echo $item->url?>" alt="" width="18%"></a>
                                        </td>
                                        <td class="cart_description">
                                            <h4><a href="product-details.php?id=<?php echo $item->id?>"><?php echo $item->name?></a></h4>
                                            <p id="webid" title="<?php echo $item->id ?>">Web ID: <?php echo $item->id?></p>
                                        </td>
                                        <td class="cart_price">
                                            <p><?php echo $item->price?>€</p>
                                        </td>
                                        <td class="cart_quantity">
                                            <div class="cart_quantity_button">
                                                <a class="cart_quantity_up" href="javascript:ajout(<?php echo $item->id ?>)"> + </a>
                                                <input class="cart_quantity_input" type="text" name="quantity" value="<?php echo $_SESSION['cart'][$item->id]?>" autocomplete="off" size="2" readonly id="<?php echo $item->id ?>">
                                                <a class="cart_quantity_down"  href="javascript:moins(<?php echo $item->id ?>)"> - </a>
                                            </div>
                                        </td>
                                        <td class="cart_total">
                                        <?php $total = ($item->price)*($_SESSION['cart'][$item->id]); ?>
                                            <p class="cart_total_price"><?php echo $total?>€</p>
                                        </td>
                                        <td class="cart_delete">
                                            <a class="cart_quantity_delete" href="javascript:eraze(<?php echo $item->id ?>)"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                $totaltva+=$total;
                                }
                                ?>
                                    
                                </tbody>
                            </table>
                      <?php }else{ ?>     
                      <table class="table table-condensed" border="0" cellspacing="0" cellpadding="0">
				<tbody><tr>
					<td><h2>Votre panier est vide !</h2></td>
					<td></td>
					<td rowspan="2">
                    <a href="./shop.php?categorie=all">
						<img src="./images/cart/bon_plan_panier_vide.jpg" alt="Tous les bons plans du moment" border="0">
					</a>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p>Si vous n'avez pas trouvé votre bonheur parmi les <strong>10000 produits sélectionnés et disponibles</strong>
							sur notre site, nous vous invitons à découvrir notre <strong>espace dédié aux "Bons plans"</strong>.
							Vous y trouverez toutes les meilleures offres du moment en un clin d'oeil !
						</p>
						<p>Alors n'hésitez plus et <strong>découvrez vite ce nouvel univers !</strong></p>
					</td>
				</tr>
			</tbody></table>      
                      
                      <?php } ?>
			</div>
		</div>
	</section> <!--/#cart_items-->
	<?php if(!empty($_SESSION['cart'])) { ?>
            <section id="do_action">
                <div class="container">
                    <div class="heading">
                        <h3>Que voulez vous faire ensuite?</h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="total_area">
                                <ul>
                                    <li>Total HT. <span><?php $totalHTVA=$totaltva-($totaltva*21/100); echo round($totalHTVA,2);?>€</span></li>
                                    <li>Frais de port <span>Gratuit</span></li>
                                    <li>Total TTC. <span><?php echo $totaltva?>€</span></li>
                                </ul>
                                    <a class="btn btn-default update" href="./shop.php">Retour aux catalogue</a>
                                    <a class="btn btn-default check_out" href="checkout.php">Paiement</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!--/#do_action-->
    <?php }?>
<script>
	function ajout(id) {
		var elem = document.getElementById(id);
		conv = parseInt(elem.value);
		conv += 1;
		elem.value = conv;
		link(id,conv);
	}
	function moins(id) {
		var elem = document.getElementById(id);
		conv = parseInt(elem.value);
		if(conv >1)
		{
			conv -= 1;
			elem.value = conv;
			link(id,conv);
		}
	}
	function link(idS, nombre) {
		window.location.href = "addtocart.php?id="+ idS +"&set=" + nombre;
	}
	function eraze(id){
		window.location.href = "addtocart.php?id="+ id +"&eraze=" + 1;
	}
</script>
	<?php
include "./includes/footer.php";
?>