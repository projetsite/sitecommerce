<?php
include "./includes/header.php";
$message ="";
if(isset($_GET['type']) && $_GET['type']=="message")
{
		if(!empty($_POST))
		{
			$result = $db->prepare("INSERT INTO contact (name, email, subject,message) VALUES (:name, :email,:subject, :message)");
			$result->execute(array('name' => $_POST['name'],'email' => $_POST['email'],'subject' =>  $_POST['subject'],'message' =>  $_POST['message']));
			$message = " Message bien enregistre, nous prendrons contact avec vous dés que possible ";
		}
}
?>
	 
	 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center">Qui sommes-<strong>nous</strong> ?</h2>
					<div id="info" class="contact-map">
                    <p><?php echo $message ?></p>
                        <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1269.941409783191!2d3.957383961899433!3d50.46190673136839!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c24fe1877bfae3%3A0x456a0e68bdf40ebd!2sHaute+Ecole+en+Hainaut+-+Campus+Technique!5e0!3m2!1sfr!2sbe!4v1421108501346"></iframe><br><small><a href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1269.941409783191!2d3.957383961899433!3d50.46190673136839!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c24fe1877bfae3%3A0x456a0e68bdf40ebd!2sHaute+Ecole+en+Hainaut+-+Campus+Technique!5e0!3m2!1sfr!2sbe!4v1421108501346" style="color:#777777;text-align:left;font-size:13px;">View Larger Map</a></small>
                       
					</div>
				</div>			 		
			</div>    	
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class="title text-center">Posez vos questions</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post" action="./contact-us.php?type=message">
				            <div class="form-group col-md-6">
				                <input type="text" name="name" class="form-control" required="required" placeholder="Nom">
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" class="form-control" required="required" placeholder="Sujet">
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" required class="form-control" rows="8" placeholder="Ton message Bro !"></textarea>
				            </div>                        
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Submit">
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Contact Info</h2>
	    				<address>
	    					<p>TechCity Inc.</p>
							<p>40 Avenue Victor Maistriau, 7000 Mons</p>
							<p>Région Hainaut, Belgique</p>
							<p>Mobile: +32 467.53.60.44</p>
							<p>Fax: 1-524-352-2004</p>
							<p>Email: info@techcity.com</p>
	    				</address>
	    			</div>
    			</div>    			
	    	</div>  
    	</div>	
    </div><!--/#contact-page-->
	
	<?php
include "./includes/footer.php";
?>