<?php
include "./includes/header.php";
$keys = $_GET['id'];
if(isset($_SESSION['login_user']))
{
		$user = $db->prepare('SELECT * FROM utilisateur WHERE mail=?');
		$user->execute(array($_SESSION['login_user']));
		$userdata = $user->fetch(PDO::FETCH_OBJ);
		if(!empty($_POST))
		{
			$result = $db->prepare("INSERT INTO commentaires (idObjet, comments, pseudo,date) VALUES (:idObjet, :comments,:pseudo, NOW())");
			$result->execute(array('idObjet' => $keys,'comments' => $_POST['comments'],'pseudo' => $userdata->pseudo));
		}
}
		$com = $db->prepare('SELECT * FROM commentaires WHERE idObjet=?');
		$com->execute(array($keys));
		$commentaire = $com->fetchAll(PDO::FETCH_OBJ);

?>
	   
	<section>
		<div class="container">
			<div class="row">

						<!--category-productsr-->
						<?php
							$keys = $_GET['id'];
							$reponse = $db->prepare('SELECT * FROM product WHERE id=?');
							$reponse->execute(array($keys));
							$data = $reponse->fetch(PDO::FETCH_OBJ);
						?>
							<!--category-productsr-->

					
				
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="<?php echo $data->url?>" alt="" />
							</div>
						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
                            <?php $new = date("Ymd")-date("Ymd",strtotime($data->date));if($new<=15){ ?>
								<img src="images/home/new.png" class="new" alt=""><?php } ?>
								<h2><?php echo $data->name?></h2>
								<p  id="webid" title="<?php echo $keys ?>">Web ID: <?php echo $keys ?></p>
								<span>
									<span><?php echo $data->price?>€</span>
									<label>Quantité:</label>
									<input type="text" value="1" id="valeur"/>
                                    <button class="btn btn-fefault cart" onClick="link()"><i class="fa fa-shopping-cart"></i>
										Ajouter au panier</button>
								</span>
								<p><b>Disponibilité:</b> En Stock</p>
								<p><b>Condition:</b> Neuf</p>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#details" data-toggle="tab">Description</a></li>
								<li ><a href="#reviews" data-toggle="tab">Avis</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="details" >
								<p><?php echo $data->description?></p>
							</div>
							<div class="tab-pane fade" id="reviews" >
								<div class="col-sm-12">
									
									<form action="product-details.php?id=<?php echo $keys?>" method="post">
										<span>
                                        <?php
											foreach ($commentaire as $coms)
											{
										?>
                                        		<p>Commentaire de <b><?php echo $coms->pseudo?></b> le <a><i class="fa fa-calendar-o"></i><?php echo $coms->date; ?></a></p>
												<textarea readonly id="example-seven"><?php echo $coms->comments ;?></textarea>
										<?php
											}
										?>
                                        <?php
										if(isset($_SESSION['login_user']))
										{
										?>
                                        <ul>
										<li><a href="./compte.php"><i class="fa fa-user"></i><?php echo $userdata->pseudo ?></a></li>
										<li><a><i class="fa fa-clock-o"></i><?php  echo date('H:i:s');?></a></li>
										<li><a><i class="fa fa-calendar-o"></i><?php echo date('d-m-Y'); ?></a></li>
										</ul>
											<p>Notez votre commentaire dans la case : </p>
										</span>
                                        
										<textarea name="comments" ></textarea>
										<button type="submit" class="btn btn-default pull-right">
											Envoyer
										</button>
									</form>
                                    <?php 
										}
										?>
								</div>
							</div>
							
						</div>
					</div><!--/category-tab-->
					
					
                    
					
				</div>
			</div>
		</div>
	</section>
    
<script>
	function link() {
					var obj = document.getElementById("webid");
					var obj2 = $("#valeur").val();
					window.location.href = "addtocart.php?id="+ obj.title +"&quantity=" + obj2;
					}
</script>
<?php
include "./includes/footer.php";
?>